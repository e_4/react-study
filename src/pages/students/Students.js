import Gradebook from '../../components/gradebook/Gradebook'
import { useThemeProps } from '@material-ui/data-grid';

export default function Students() {
  const groups = [
    {
      id: "36012",
      year: 3
    },
    {
      id: "36203",
      year: 3
    },
    {
      id: "42011М",
      year: 3
    }
  ];

  const students = [
    {
      id: 1,
      group: "36012",
      name: "Иван Иванов"
    },
    {
      id: 2,
      group: "36012",
      name: "Петр Петров"
    },
    {
      id: 3,
      group: "36012",
      name: "Александра Иванова"
    },
    {
      id: 4,
      group: "36012",
      name: "Константин Константинопольский"
    },
    {
      id: 5,
      group: "36012",
      name: "Сидор Сидоров"
    },
    {
      id: 6,
      group: "36012",
      name: "Мария Иванова"
    }

  ];

  const marksByDate = [
    {      
      date: "2021-03-16",
      marks: [
        {
          student: 1,
          mark: 2
        }
      ]
    }
  ];

  const notices = [

  ];
  
  return (
    <div className="page">
      <Gradebook groups={groups} students={students}/>
    </div>      
  );
}