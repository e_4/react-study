import LessonsGrid from '../../components/lessons-grid/LessonsGrid'

export default function Schedule() {
  
    const groups = [
      {
        id: "36012",
        year: 3
      },
      {
        id: "36203",
        year: 3
      },
      {
        id: "42011М",
        year: 3
      }
    ];

    const lessons = [
      { 
        id: 0,
        name: "Теор. вер",
        location: "201",
        date: "2021-03-12",
        time: "13:30",
        description: "",
        repeat: "everyweek",
        year: 3
      },
      { 
        id: 1,
        name: "Лин. анализ",
        location: "215",
        date: "2021-03-12",
        time: "11:10",
        description: "",
        repeat: "everyweek",
        groups: ["36012"]
      },
      { 
        id: 2,
        name: "Лин. анализ",
        location: "100-м",
        date: "2021-03-14",
        time: "13:30",
        description: "",
        repeat: "everyweek",
        groups: ["36203"]
      },
      { 
        id: 3,
        name: "Мат. стат.",
        location: "205",
        date: "2021-03-16",
        time: "13:30",
        description: "",
        repeat: "everyweek",
        groups: ["42011М"]
      }
    ];

    return (
      <div className="page">        
        <LessonsGrid lessons={lessons} groups={groups}/>
      </div>
    );
}