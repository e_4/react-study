import React, { Component } from 'react'
import Box from '@material-ui/core/Box';

export default class Scroller extends Component {
    componentDidMount() {
        if (this.props.position) {
            this.setScrollPosition(this.props.position)
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.position !== prevProps.position) {
            this.setScrollPosition(this.props.position)
        }
    }

    handleScrollerRef = (reference) => { this.scroller = reference }

    setScrollPosition(position = 0) {
        this.scroller.scrollLeft = position
    }

    handleScroll = () => {
        const leftEdge = this.scroller.firstChild.offsetLeft
        const rightEdge = this.scroller.lastChild.offsetLeft + this.scroller.lastChild.offsetWidth
        const scrolledLeft = this.scroller.scrollLeft + this.scroller.offsetLeft
        const scrolledRight = this.scroller.scrolledLeft + this.scroller.offsetWidth

        if (scrolledRight >= rightEdge) {
            this.props.onReachRight()
        } else if (scrolledLeft <= leftEdge) {
            this.props.onReachLeft()
        }
    }

    render() {
        return (
            <Box
                ref={this.handleScrollerRef}
                style={{
                    overflow: 'auto',
                    height: 'inherit',
                    width: 'inherit',
                    whiteSpace: 'nowrap'
                }}
                onScroll={this.handleScroll}>
                {this.props.children}
            </Box>
        )
    }
}