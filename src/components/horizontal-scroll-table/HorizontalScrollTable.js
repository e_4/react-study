import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Scroller from './Scroller'

export default class HorizontalScrollTable extends Component {
    constructor(props) {
        super(props);
    }

    getColumn = (columnData) => {       
        return (
            <Box key={columnData.key}>
                {columnData.values.map(value => (
                    <Box>value.mark</Box>
                ))}
            </Box>
        )
    }

    handleScrollLeft = () => {
        const items = [].concat(this.getItems()).concat(this.state.items)
        this.setState({ items })
    }

    handleScrollRight = () => {
        const items = this.state.items.concat(this.getItems())
        this.setState({ items })
    }

    render() {
        return (
            <Grid container>
                <Grid item xs={3}>
                    
                </Grid>
                <Grid item xs={8}>
                    <Scroller
                        onReachLeft={this.handleScrollLeft}
                        onReachRight={this.handleScrollRight}
                        position={10}
                    >
                        {this.state.items}
                    </Scroller>
                </Grid>
            </Grid>
        )
    }
}