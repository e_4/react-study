import * as React from 'react';
import { DataGrid, GridToolbarContainer } from '@material-ui/data-grid';
import StudyGroupSelector from '../../components/study-group-selector/StudyGroupSelector';

function CustomToolbar(props) {
  return (
    <GridToolbarContainer>
      <StudyGroupSelector groups={props.groups} onSelectGroup={props.onSelectGroup}/>
    </GridToolbarContainer>
  );
}

export default function LessonsGrid(props) {
  const rows = props.lessons;
  const groups = props.groups;
  const [selectedGroup, setSelectedGroup] = React.useState(null);

  const handleSelectGroup = (group) => {
    alert(group);
    setSelectedGroup(group);
  }

  const columns = [
    { field: 'id', headerName: 'id', hide: true },
    { field: 'groups', headerName: 'Группа', 
      valueFormatter: (params) => params.value,
    },
    {
      field: 'date', headerName: 'Дата', width: 130,
      valueFormatter: (params) => params.value,
    },
    { field: 'weekday', headerName: 'День недели', width: 130 },
    { field: 'time', headerName: 'Время', width: 130 },
    {
      field: 'info',
      headerName: 'Full info',
      sortable: false,
      width: 400,
      valueGetter: (params) =>
        `${params.getValue('name') || ''}, ауд. ${params.getValue('location') || ''}, ${params.getValue('groups') || params.getValue('year') + ' курс'}`,
    },
  ];

  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid rows={rows} columns={columns} pageSize={5} hideFooter headerHeight={0} 
      components={{ Toolbar: CustomToolbar, }} 
      componentsProps={{ toolbar: { groups: groups, onSelectGroup: handleSelectGroup } }}
      />
    </div>
  );
}