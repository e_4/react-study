import Grid from '@material-ui/core/Grid';

export default function LessonGridItem(props) {
  
    return (
        <Grid container spacing={5}>
            <Grid container item xs={6} spacing={3}>
                {props.item.date}
            </Grid>
            <Grid container item xs={6} spacing={3}>
                {props.item.name}
            </Grid>
        </Grid>    
    );
}