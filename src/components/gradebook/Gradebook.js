import * as React from 'react';
import StudyGroupSelector from '../../components/study-group-selector/StudyGroupSelector'
import HorizontalScrollTable from '../horizontal-scroll-table/HorizontalScrollTable'

export default function Gradebook(props) {
    const rows = props.students.map((student) => ({ id: student.id, name: student.name}));
    return (
        <div>
            <StudyGroupSelector groups={props.groups}/>
            <HorizontalScrollTable rows={rows}/>
        </div>
    );
}