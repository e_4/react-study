import React, { Component } from 'react';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

export default class StudyGroupSelector extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(id, e) {   
     e.preventDefault();
     this.props.onSelectGroup(id);
  }

  render() {
    const buttons = this.props.groups.map((group) => 
      <Button key={group.id} onClick={(e) => this.handleClick(group.id, e)} >{group.id}</Button>  );

    return(
      <div className="study-group-selector">
        <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
          <Button onClick={(e) => this.handleClick(null, e)}>Все</Button>
          {buttons}
        </ButtonGroup>
      </div>
    )
  }
}